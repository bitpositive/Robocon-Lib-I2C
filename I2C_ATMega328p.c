
/*!
  @file   I2C_ATMega328p.c
  @author  T.Kawamura
  @date   Tue Dec 27 21:44:42 2016
  
  @brief  ATMega328P用I2C通信ライブラリ
  
  I2C通信でデータをやり取りするライブラリです。

  @version	Alpha 1.0
  @date		2016-12-27	T.Kawamura	新規作成
  @copyright	(C) 2016 T.Kawamura All Rights Reserved.
*/

#include <avr/io.h>
#include <avr/sfr_defs.h>
#include <util/twi.h>
#include "I2C_ATMega328p.h"

/*
  The TWI can operate in one of four major modes:
  • Master Transmitter (MT)
  • Master Receiver (MR)
  • Slave Transmitter (ST)
  • Slave Receiver (SR)
*/

/*!
  @brief	I2C通信の初期化

  I2C通信の初期設定を行い、通信を始める準備を整えます。

  @param	なし
  @return	なし
*/
void I2C_Init(void){
  TWBR = TWBR_VALUE;	// ビットレート設定
  TWSR = TWPS_VALUE;	// 分周器設定
  TWCR = _BV(TWEN);	// TWI動作許可・TWINTをクリア
}

/*! 
  @brief スタートコンディション発行とSLA+W/R送信

	I2Cのスタートコンディションを発行したあと、SLA+W/Rを送信する関数です。

	@param		address		スレーブのアドレス(Write/Readは含まない、7bitの純粋なアドレス！)
	@param		readOrWrite	マスターから見てReadなのかWriteなのかを指定(マクロ MASTER_WRITE または MASTER_READで指定すること)
	
  @retval		I2C_NG		エラー
	@retval		I2C_OK		成功
*/
uint8_t I2C_SendSLA(uint8_t address, uint8_t readOrWrite){
  uint8_t sla;

  TWCR |= _BV(TWINT) | _BV(TWSTA);	// TWINTに１を代入することでTWINTの値をクリア（＝０にする）し、TWIに関する動作が行える状態にしたあとで、TWSTAを立てることでスタートコンディションを発行する
	
  while( !( TWCR & _BV(TWINT) ) );	// TWINTが立つまで（処理が終わるまで）待機
  if( ( TW_STATUS != TW_START ) && ( TW_STATUS != TW_REP_START ) ){	// スタートコンディションまたはリスタートコンディションが発行されているかを確認、されていなければエラーを返す
    return I2C_NG;
  }

  sla = ( address << 1 ) | ( readOrWrite & 0x01 );	// 実際に送信するデータを作成

  TWDR = sla;	// データレジスタに書き込み
  TWCR &= ~_BV(TWSTA);
  TWCR |= _BV(TWINT);	// TWINTをクリアしデータレジスタの内容を送信させる

  while( !( TWCR & _BV(TWINT) ) );	// TWINTが立つまで（処理が終わるまで）待機
  if( ( TW_STATUS != TW_MT_SLA_ACK ) || ( TW_STATUS != TW_MR_SLA_ACK ) ){	// ACKが返ってきたかを確認、返っていなければエラーを返す
    return I2C_NG;
  }
	
  return I2C_OK;
}

/*! 
	@brief		データ送信

	ポインタで渡された配列 dataArrayの[0]から順に、データをdataSizeの分だけ送信します。
	必ず関数I2C_SendSLA()を実行したあとに実行すること。

	@param		*dataArray		送信するデータが[0]から順に入っている配列（ポインタで渡す。例えばデータの入った配列をArrayとすると、 &Array[0] または Array を引数とする。）
	@param		dataSize			送信するデータのサイズ(Byte)
	
  @retval		I2C_NG		エラー
	@retval		I2C_OK		成功
*/
uint8_t I2C_WriteData(uint8_t *dataArray, uint16_t dataSize){
  uint16_t i;

  for( i = 0; i < dataSize; i++ ){

    TWDR = *dataArray++;
    TWCR |= _BV(TWINT);	// TWINTをクリアしデータレジスタの内容を送信させる
  
    while( !( TWCR & _BV(TWINT) ) );	// TWINTが立つまで（処理が終わるまで）待機
    if( TW_STATUS != TW_MT_DATA_ACK ){	// ACKが返ってきたかを確認、されていなければエラーを返す
			return I2C_NG;
    }

  }

  return I2C_OK;
}

/*! 
	@brief		データ受信

	データをdataSizeの分だけ受信し、ポインタで渡された配列 dataArrayの[0]から順に格納します。
	必ず関数I2C_SendSLA()を実行したあとに実行すること。

	@param		*dataArray		受信するデータを格納する配列（ポインタで渡す。例えばデータの入った配列をArrayとすると、 &Array[0] または Array を引数とする。） 
	@param		dataSize		受信するデータのサイズ(Byte)
	
  @retval		I2C_NG		エラー
	@retval		I2C_OK		成功
*/
uint8_t I2C_ReadData(uint8_t *dataArray, uint16_t dataSize){
  uint16_t i;

  if( dataSize < 1 ){		// dataSizeが1未満などありえないので弾く
    return I2C_NG;
  }

	/* 最後の１Byteのデータ受信まではACKを返す*/
	for( i = 0; i < ( dataSize - 1 ); i++ ){
    TWCR |= _BV(TWINT) | _BV(TWEA);
    
    while( !( TWCR & _BV(TWINT) ) );	// TWINTが立つまで（スレーブによる1Byteのデータ送信が終わるまで）待機する
    if( TW_STATUS != TW_MR_DATA_ACK ){	// 1Byteのデータを受信しACKを返したかを確認、されていなければエラーを返す
      return I2C_NG;
    }
    *dataArray++ = TWDR;  
  }

	/* 最後の１Byteのデータ受信なのでNACKを返す*/
  TWCR |= _BV(TWINT);
  
  while( !( TWCR & _BV(TWINT) ) );	// TWINTが立つまで（スレーブによる1Byteのデータ送信が終わるまで）待機する
  if( TW_STATUS != TW_MR_DATA_NACK ){	// 1Byteのデータを受信しNACKを返したかを確認、されていなければエラーを返す
    return I2C_NG;
  }
  *dataArray++ = TWDR;

  return I2C_OK;
}

/*! 
	@brief		ストップコンディション発行

	ストップコンディションを強制的に発行します。

	@param		なし

	@return		なし
 */
void I2C_Stop(void){
	TWCR |= _BV(TWINT) | _BV(TWSTO);
}

/*! 
	@brief		指定した分だけ指定したデータを送信、その後指定した分だけデータを受信する

	まずはじめにスタートコンディションを発行したあと、SLA+Wを送ります。つぎに送信データを指定した分だけ順に送信します。
	それが終わったらリピートスタートコンディションを発行し、SLA+Rを送ります。最後に指定した分だけデータを受信し、ストップコンディションを発行します。

	実際にI2C通信を利用する場合は、全てこの関数一つで事足りるはずです。マスターなら。

	@param		address			通信するスレーブのアドレス（純粋な7bitのアドレスのみ。W/Rは含まない！）
	@param		*txData			送信するデータが[0]から順に入っている配列（ポインタで渡す。例えばデータの入った配列をArrayとすると、 &Array[0] または Array を引数とする。） 
	@param		txDataSize	送信するデータのサイズ(Byte)
	@param		*rxData			受信するデータを格納する配列（ポインタで渡す。例えばデータの入った配列をArrayとすると、 &Array[0] または Array を引数とする。） 
	@param		rxDataSize	受信するデータのサイズ(Byte)
	
	@retval		I2C_NG		エラー
	@retval		I2C_OK		成功 
*/
uint8_t I2C_SendAndReceive(uint8_t address, uint8_t *txData, uint8_t txDataSize, uint8_t *rxData, uint8_t rxDataSize ){
	uint8_t result = I2C_OK;

	/* Start */
	result = I2C_SendSLA(address, MASTER_WRITE);
	if( result == I2C_NG ){
		return result;
	}

	/* Write Data */
	result = I2C_WriteData(txData, txDataSize);
	if( result == I2C_NG ){
		return result;
	}

	/* Restart */
	result = I2C_SendSLA(address, MASTER_READ);	
	if( result == I2C_NG ){
		return result;
	}

	/* Read Data */
	result = 	I2C_ReadData(rxData, rxDataSize);
	if( result == I2C_NG ){
		return result;
	}

	/* Stop */
	I2C_Stop();

	return I2C_OK;
}
